import "./index.css"
import Login from './views/login/index'
import Dashboard from "./views/user/dashboard"
import Profile from "./views/user/profile"
import Menu from "./views/user/Menu"
import Notification from "./views/user/Notification"
import History from "./views/user/History"
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom'

function App() {
    return (
        <>
        <Router>
            <Routes>
                <Route path="/Auth" element={<Login/>}/>
                <Route path="/Dashboard" element={<Dashboard/>}/>
                <Route path="/Menu" element={<Menu/>}/>
                <Route path="/Notification" element={<Notification/>}/>
                <Route path="/History" element={<History/>}/>
                <Route path="/Profile" element={<Profile/>}/>
            </Routes>
        </Router>
        </>
    )
}

export default App
