const tabList = () => {
    return (
        <div role="tablist" className="flex flex-row items-center tabs tabs-bordered">
            <div role="tab" className="tab tab-active">All Order</div>
            <div role="tab" className="tab">On Going</div>
            <div role="tab" className="tab">Completed</div>
            <div role="tab" className="tab">Canceled</div>
        </div>
    )
}

export default tabList;