import {CiCircleInfo} from "react-icons/ci";

const Table = () => {
    return (
        <div className="overflow-x-auto rounded-2xl shadow-xl bg-[#F6F5F5]">
            <table className="table table-sm">
                {/* head */}
                <thead className="bg-[#99BC85]">
                    <tr>
                        {/* <th>
                            <label>
                                <input type="checkbox" className="checkbox"/>
                            </label>
                        </th> */}
                        <th className="text-xs text-white">Canteen</th>
                        <th className="text-xs text-white">Order</th>
                        <th className="text-xs text-white">Total payment</th>
                        <th className="text-xs text-white">Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    {/* row 1 */}
                    <tr className="hover:bg-zinc-200 hover:cursor-pointer">
                        {/* <th>
                            <label>
                                <input type="checkbox" className="checkbox"/>
                            </label>
                        </th> */}
                        <td>
                            <div className="flex items-center gap-3">
                                <div className="avatar">
                                    <div className="mask mask-squircle w-8">
                                        <img
                                            src="https://miro.medium.com/v2/resize:fit:1134/0*4bjUTfx-yFBOn2Bd.jpg"
                                            alt="Avatar Tailwind CSS Component"/>
                                    </div>
                                </div>
                                <div>
                                    <div className="font-bold text-xs">Kantin Pak Ryan</div>
                                    <div className="text-xs opacity-50">Kantin 1</div>
                                </div>
                            </div>
                        </td>
                        <td className="text-xs">
                            Croissant
                            <br/>
                            <span className="text-xs">21 December 2024</span>
                        </td>
                        <td className="text-xs">Rp8.000</td>
                        <td className="text-xs">2</td>
                    </tr>
                    {/* row 2 */}
                    <tr className="hover:bg-zinc-200 hover:cursor-pointer">
                        {/* <th>
                            <label>
                                <input type="checkbox" className="checkbox"/>
                            </label>
                        </th> */}
                        <td>
                            <div className="flex items-center gap-3">
                                <div className="avatar">
                                    <div className="mask mask-squircle w-8">
                                        <img
                                            src="https://miro.medium.com/v2/resize:fit:1134/0*4bjUTfx-yFBOn2Bd.jpg"
                                            alt="Avatar Tailwind CSS Component"/>
                                    </div>
                                </div>
                                <div>
                                    <div className="font-bold text-xs">Kantin Bu Retno</div>
                                    <div className="text-xs opacity-50">Kantin 2</div>
                                </div>
                            </div>
                        </td>
                        <td className="text-xs">
                            Chicken Noodle
                            <br/>
                            <span className="text-xs">21 December 2024</span>
                        </td>
                        <td className="text-xs">Rp10.000</td>
                        <td className="text-xs">1</td>
                    </tr>
                    {/* row 3 */}
                    <tr className="hover:bg-zinc-200 hover:cursor-pointer">
                        {/* <th>
                            <label>
                                <input type="checkbox" className="checkbox"/>
                            </label>
                        </th> */}
                        <td>
                            <div className="flex items-center gap-3">
                                <div className="avatar">
                                    <div className="mask mask-squircle w-8">
                                        <img
                                            src="https://miro.medium.com/v2/resize:fit:1134/0*4bjUTfx-yFBOn2Bd.jpg"
                                            alt="Avatar Tailwind CSS Component"/>
                                    </div>
                                </div>
                                <div>
                                    <div className="font-bold text-xs">Kantin Bu Diah</div>
                                    <div className="text-xs opacity-50">Kantin 3</div>
                                </div>
                            </div>
                        </td>
                        <td className="text-xs">
                            Beng-Beng
                            <br/>
                            <span className="text-xs">24 December 2024</span>
                        </td>
                        <td className="text-xs">Rp4.000</td>
                        <td className="text-xs">4</td>
                    </tr>
                    {/* row 4 */}
                    <tr className="hover:bg-zinc-200 hover:cursor-pointer">
                        {/* <th>
                            <label>
                                <input type="checkbox" className="checkbox"/>
                            </label>
                        </th> */}
                        <td>
                            <div className="flex items-center gap-3">
                                <div className="avatar">
                                    <div className="mask mask-squircle w-8">
                                        <img
                                            src="https://miro.medium.com/v2/resize:fit:1134/0*4bjUTfx-yFBOn2Bd.jpg"
                                            alt="Avatar Tailwind CSS Component"/>
                                    </div>
                                </div>
                                <div>
                                    <div className="font-bold text-xs">Kantin Bugal</div>
                                    <div className="text-xs opacity-50">Kantin 4</div>
                                </div>
                            </div>
                        </td>
                        <td className="text-xs">
                            Chicken Katsu
                            <br/>
                            <span className="text-xs">26 December 2024</span>
                        </td>
                        <td className="text-xs">Rp25.000</td>
                        <td className="text-xs">2</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default Table;
