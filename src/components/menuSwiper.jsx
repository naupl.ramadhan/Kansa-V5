import {Swiper, SwiperSlide} from 'swiper/react';
import { FreeMode } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/pagination';
import burger from '../assets/burger.svg';
import ice from '../assets/iceCream.svg';
import drink from '../assets/drink.svg';
import fries from '../assets/fries.svg';
import meat from '../assets/chicken.svg';
import cuisine from '../assets/cuisine.svg';
import cake from '../assets/cake.svg';

const Anigas = () => {
    return (
        <div>
            <Swiper
                slidesPerView={'auto'}
                spaceBetween={0}
                freeMode={true}
                effect={'creative'}
                modules={[FreeMode]}
                className="mySwiper w-full h-full overflow-hidden mb-10  rounded-3xl relative hidden lg:block">
                <SwiperSlide style={{
                    width: '125px'
                }}>
                    <div>
                        <div
                            className="flex flex-col justify-center bg-gray-100 rounded-2xl h-[150px] transition duration-100 ease-in-out transform scale-90 hover:scale-75 hover:outline">
                            <a
                                href=""
                                className='flex flex-col items-center gap-3 '>
                                <img
                                    src={burger}
                                    alt="Burger"
                                    className="icon w-16 mx-auto outline-none"/>
                                <label className='text-md text-lg'>Fast Foods</label>
                            </a>
                        </div>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: '125px'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 rounded-2xl h-[150px] transition duration-100 ease-in-out transform scale-90 hover:scale-75 hover:outline">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3'>
                            <img
                                src={ice}
                                alt="Ice Cream"
                                className="icon w-16 mx-auto outline-none"/>
                            <label className='text-md text-lg'>Sweets</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: '125px'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 rounded-2xl h-[150px] transition duration-100 ease-in-out transform scale-90 hover:scale-75 hover:outline">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3'>
                            <img
                                src={drink}
                                alt="Drink"
                                className="icon w-16 mx-auto outline-none"/>
                            <label className='text-md text-lg'>Drinks</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: '125px'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 rounded-2xl h-[150px] transition duration-100 ease-in-out transform scale-90 hover:scale-75 hover:outline">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3'>
                            <img
                                src={fries}
                                alt="Fries"
                                className="icon w-16 mx-auto outline-none"/>
                            <label className='text-md text-lg'>Snacks</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: '125px'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 rounded-2xl h-[150px] transition duration-100 ease-in-out transform scale-90 hover:scale-75 hover:outline">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3'>
                            <img
                                src={meat}
                                alt="Meat"
                                className="icon w-16 mx-auto outline-none"/>
                            <label className='text-md text-lg'>Meats</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: '125px'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 rounded-2xl h-[150px] transition duration-100 ease-in-out transform scale-90 hover:scale-75 hover:outline">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3'>
                            <img
                                src={cuisine}
                                alt="Cuisine"
                                className="icon w-16 mx-auto outline-none"/>
                            <label className='text-md text-lg'>Cuisines</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: '125px'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 rounded-2xl h-[150px] transition duration-100 ease-in-out transform scale-90 hover:scale-75 hover:outline">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3'>
                            <img
                                src={cake}
                                alt="Cake"
                                className="icon w-16 mx-auto outline-none"/>
                            <label className='text-md text-lg'>Cakes</label>
                        </a>
                    </div>
                </SwiperSlide>
            </Swiper>
        </div>
    );
};

export default Anigas;
