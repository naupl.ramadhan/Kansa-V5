import {useState} from 'react';
import {useNavigate} from 'react-router-dom';
import {IoEye, IoEyeOff} from "react-icons/io5";

const profileBalance = () => {

    const [showBalance,
        setShowBalance] = useState(false);

    const toggleBalanceVisibility = () => {
        setShowBalance(!showBalance);
    }

    const navigate = useNavigate();
    const Dashboard = () => {
        navigate("/Dashboard");
    };

    return (
        <div className="flex flex-col lg:flex-row gap-4">
            <div
                className="card flex flex-col bg-gradient-to-r from-green-400 via-blue-500 to-purple-600 text-white shadow-xl rounded-xl p-4 gap-4 border w-full lg:w-1/2">
                <div className="flex flex-row justify-between items-center gap">
                    <div className="font-semibold text-xs lg:text-2xl">K-Wallet</div>
                    <div className="text-xs lg:text-md text-white">1234567890</div>
                </div>

                <div className="flex flex-row items-center gap-4">
                    {showBalance
                        ? (<IoEyeOff
                            className="hover:cursor-pointer"
                            size={20}
                            onClick={toggleBalanceVisibility}/>)
                        : (<IoEye
                            className="hover:cursor-pointer"
                            size={20}
                            onClick={toggleBalanceVisibility}/>)}
                    <div className="text-white text-sm lg:text-xl">
                        {showBalance
                            ? "Rp1.000.000"
                            : "••••••••••"}
                    </div>
                </div>
            </div>
            <div className="flex flex-col gap-4 justify-center w-full lg:w-1/2">
                <button
                    className="btn rounded-full font-semibold bg-green-500 hover:bg-green-500 text-white focus:outline focus:outline-offset-1">
                    Save
                </button>
                <button
                    onClick={Dashboard}
                    className="btn rounded-full font-semibold bg-gray-200 hover:text-white hover:bg-red-500 focus:outline focus:outline-offset-1">
                    Nevermind
                </button>
            </div>
        </div>
    )
}

export default profileBalance;