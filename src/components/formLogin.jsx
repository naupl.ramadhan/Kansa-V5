import {useNavigate} from "react-router-dom";
import {CiUser, CiLock} from "react-icons/ci";

const FormLogin = () => {
    const navigate = useNavigate();
    const loginHandler = () => {
        navigate("/Dashboard");
    };

    return (
        <form method="POST" id="login" className="w-full h-full bg-white lg:bg-transparent p-10 lg:p-20 lg:-mt-28 rounded-t-[30px] shadow-md lg:shadow-none">
            <div className="flex flex-col items-center mb-8 lg:mb-5">
                <p
                    className="flex justify-center lg:justify-start font-bold text-gray-800 text-md lg:text-lg dark:text-white">Welcome back!</p>
                <p
                    className="flex justify-center lg:justify-start text-xs text-gray-600">please Log In to continue with us</p>
            </div>
            <div className="mb-4">
                <label
                    className="block text-gray-700 text-sm lg:text-md mb-2 dark:text-white">Username</label>
                <div className="relative mb-4">
                    <div
                        className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                        <CiUser size={25}/>
                    </div>
                    <input
                        type="text"
                        id="username"
                        className="input input-bordered input-sm pl-11 w-full lg:input-md lg:w-full lg:pl-11 outline outline-1 outline-gray-300"
                        placeholder="example : user"
                        autoComplete="off"
                        required/>
                </div>
            </div>
            <div className="mb-7">
                <label
                    className="block text-gray-700 text-sm lg:text-md mb-2 dark:text-white">Password</label>
                <div className="relative mb-4">
                    <div
                        className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                        <CiLock size={25}/>
                    </div>
                    <input
                        type="password"
                        name="pass"
                        id="password"
                        className="input input-bordered input-sm pl-11 w-full lg:input-md lg:w-full lg:pl-11 outline outline-1 outline-gray-300"
                        placeholder="example : 123"
                        required/>
                </div>
            </div>
            <div className="flex flex-col mb-5 lg:mb-4">
                <button
                    id="loginButton"
                    onClick={loginHandler}
                    className="btn btn-sm lg:btn-md rounded-full font-semibold bg-green-500 hover:bg-green-500 text-white focus:outline focus:outline-offset-1 g-recaptcha"
                    data-sitekey="6LcIEVooAAAAANWXcerxsKOuRAjo1sIVQfldVdQx"
                    data-callback="onSubmit"
                    data-action="submit">
                    <span id="buttonText" className="inter font-bold">Log In</span>
                    <span id="loadingIndicator" className="loading loading-dots loading-md hidden"></span>
                </button>
            </div>
            <div
                className="flex justify-center text-xs text-gray-700 dark:text-white">Kansa © 2023 - All right reserved</div>
        </form>
    );
}

export default FormLogin;
