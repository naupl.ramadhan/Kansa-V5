import { CiHome, CiGrid41, CiClock2, CiLogout } from "react-icons/ci";
import logoNegative from "../images/logoNegative.png";
import {useNavigate} from "react-router-dom";

const Sidebar = () => {
    const navigate = useNavigate();
    const Dashboard = () => {
        navigate("/Dashboard");
    };
    const Menu = () => {
        navigate("/Menu");
    };
    const History = () => {
        navigate("/History");
    };
    const logoutHandler = () => {
        navigate("/Auth");
    };

    return (
        <div className="fixed top-0 left-0 h-full w-fit flex items-center">
            <div className="sidebar bg-white lg:flex flex-col items-center ml-12 rounded-2xl shadow-md hidden">
                {/* logo */}
                <div className="-mb-2">
                    <img src={logoNegative} width="60px" height="60px"/>
                </div>

                {/* menu */}
                <ul className="menu menu-vertical gap-2">
                    <li className="lg:tooltip lg:tooltip-right" data-tip="Dashboard">
                        <a
                            onClick={Dashboard}
                            className="w-fit h-[55px] flex items-center mx-auto space-x-2 transition duration-100 ease-in-out transform hover:scale-95 text-gray-700 hover:bg-[#99BC85] hover:text-white rounded-2xl">
                            <CiHome size={25}/>
                        </a>
                    </li>
                    <li className="lg:tooltip lg:tooltip-right" data-tip="Menu">
                        <a
                            onClick={Menu}
                            className="w-fit h-[55px] flex items-center mx-auto space-x-2 transition duration-100 ease-in-out transform hover:scale-95 text-gray-700 hover:bg-[#99BC85] hover:text-white rounded-2xl">
                            <CiGrid41 size={25}/>
                        </a>
                    </li>
                    <li className="lg:tooltip lg:tooltip-right" data-tip="History">
                        <a
                            onClick={History}
                            className="w-fit h-[55px] flex items-center mx-auto space-x-2 transition duration-100 ease-in-out transform hover:scale-95 text-gray-700 hover:bg-[#99BC85] hover:text-white rounded-2xl">
                            <CiClock2 size={25}/>
                        </a>
                    </li>
                    <li>
                        <a
                            onClick={logoutHandler}
                            className="w-fit h-[55px] flex items-center mx-auto space-x-2 transition duration-100 ease-in-out transform hover:scale-95 bg-red-500 hover:bg-gray-300 rounded-2xl shadow-xl">
                            <CiLogout size={25} className="text-gray-100"/>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default Sidebar;
