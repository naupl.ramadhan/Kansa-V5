import {useState} from 'react';
import {CiSearch, CiSettings, CiBellOn} from "react-icons/ci";
import {useNavigate} from 'react-router-dom';
import Notification from '../views/user/Notification';

const SearchBar = () => {
    const [searchTerm,
        setSearchTerm] = useState('');

    const handleSearch = (event) => {
        setSearchTerm(event.target.value);
    };

    const navigate = useNavigate();
    const Profile = () => {
        navigate("/Profile");
    };

    const Notification = () => {
        navigate("/Notification");
    };

    return (
        <div className='flex justify-between items-center gap-5 pt-7'>
            <div className="relative w-full lg:pl-8 lg:pr-0 pl-10 pr-10">
                <label
                    className="input input-bordered flex justify-between items-center w-full input-md lg:input-md lg:w-full outline-double outline-offset-4 outline-1 outline-gray-300 rounded-full">
                    <input
                        type="text"
                        id="username"
                        value={searchTerm}
                        onChange={handleSearch}
                        className="growl"
                        placeholder="Search here"
                        autoComplete="off"/>
                    <CiSearch
                        className="lg:block hidden text-gray-500 space-x-2 transition duration-100 ease-in-out transform hover:scale-110 hover:text-gray-800 hover:cursor-pointer"
                        size={30}/>
                    <CiSearch
                        className="lg:hidden block text-gray-500 space-x-2 transition duration-100 ease-in-out transform hover:scale-110 hover:text-gray-800 hover:cursor-pointer"
                        size={18}/>
                </label>
            </div>
            <div className='lg:flex gap-4 pr-10 items-center hidden'>
                <div className="">
                    <CiBellOn
                        className="text-gray-500 mt-1 space-x-2 transition duration-100 ease-in-out transform hover:scale-110 hover:text-gray-800 hover:cursor-pointer"
                        size={30} onClick={Notification}/>
                </div>

                <div className="">
                    <CiSettings
                        className="text-gray-500 mt-1 space-x-2 transition duration-100 ease-in-out transform hover:scale-110 hover:text-gray-800 hover:cursor-pointer"
                        size={30}/>
                </div>

                <a onClick={Profile}>
                    <div className="avatar">
                        <div
                            className="w-14 rounded-full ring ring-green-500 ring-offset-base-100 ring-offset-2 hover:ring-gray-800 hover:cursor-pointer">
                            <img src="https://miro.medium.com/v2/resize:fit:1134/0*4bjUTfx-yFBOn2Bd.jpg"/>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    );
};

export default SearchBar;
