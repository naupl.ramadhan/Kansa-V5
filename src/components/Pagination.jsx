import {CiCalendarDate, CiShoppingTag, CiCircleChevLeft, CiCircleChevRight} from "react-icons/ci";

const Pagination = () => {
    return (
        <div className="flex flex-row items-center gap-3">
            <div className="join join-horizontal shadow-lg">
                <button className="join-item btn btn-xs"><CiCircleChevLeft size={20}/></button>
                <button className="join-item btn btn-xs">Page 1</button>
                <button className="join-item btn btn-xs"><CiCircleChevRight size={20}/></button>
            </div>
            <ul className="menu bg-base-200 shadow-lg lg:menu-horizontal rounded-box">
                <li>
                    <a className="flex flex-row items-center text-xs font hover:bg-[#99BC85] hover:text-white">
                        <CiCalendarDate size={18}/>
                        Sort by date
                    </a>
                </li>
                <li>
                    <a className="flex flex-row items-center text-xs hover:bg-[#99BC85] hover:text-white">
                        <CiShoppingTag size={18}/>
                        Sort by Price
                    </a>
                </li>
            </ul>
        </div>
    )
}

export default Pagination;