import {Swiper, SwiperSlide} from 'swiper/react';
import {Autoplay, Pagination} from 'swiper/modules';
import firstB from "../images/bannerOne.png"
import secB from "../images/bannerTwo.png"
import thrB from "../images/bannerThree.png"
import fourB from "../images/bannerFour.png"
import 'swiper/css/pagination';
import 'swiper/css';

const Slider = () => {
    return (
        <Swiper
            slidesPerView={'1'}
            spaceBetween={20}
            loop={true}
            autoplay={{
            delay: 1850,
            disableOnInteraction: false
        }}
            pagination={{
            clickable: true
        }}
            effect={'creative'}
            modules={[Autoplay, Pagination]}
            className="swiper lg:w-full lg:mb-10 container overflow-hidden rounded-3xl relative">
            <SwiperSlide className="rounded-3xl overflow-hidden relative">
                <img
                    src={firstB}
                    className="w-full min-h-[190px] lg:h-[300px] object-cover rounded-3xl"/>
                <div className="absolute inset-0 bg-black opacity-25 rounded-3xl"></div>
            </SwiperSlide>
            <SwiperSlide className="rounded-3xl overflow-hidden relative">
                <img
                    src={secB}
                    className="w-full min-h-[190px] lg:h-[300px] object-cover rounded-3xl"/>
                <div className="absolute inset-0 bg-black opacity-25 rounded-3xl"></div>
            </SwiperSlide>
            <SwiperSlide className="rounded-3xl overflow-hidden relative">
                <img
                    src={thrB}
                    className="w-full min-h-[190px] lg:h-[300px] object-cover rounded-3xl"/>
                <div className="absolute inset-0 bg-black opacity-25 rounded-3xl"></div>
            </SwiperSlide>
            <SwiperSlide className="rounded-3xl overflow-hidden relative">
                <img
                    src={fourB}
                    className="w-full min-h-[190px] lg:h-[300px] object-cover rounded-3xl"/>
                <div className="absolute inset-0 bg-black opacity-25 rounded-3xl"></div>
            </SwiperSlide>
        </Swiper>
    );
};

export default Slider;
