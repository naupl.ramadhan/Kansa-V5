import { CiFacebook, CiTwitter } from "react-icons/ci";

const Icon = () => {
    return (
        <div
            className="flex justify-center space-x-8 lg:space-x-6 mb-10 lg:mb-4 text-4xl lg:text-3xl">
            <a
                href="#"
                className="text-blue-500 transition duration-100 ease-in-out transform hover:scale-110"
                title="Twitter">
                <CiFacebook/>
            </a>
            <a
                href="#"
                className="text-blue-500 transition duration-100 ease-in-out transform hover:scale-110"
                title="Facebook">
                <CiTwitter/>
            </a>
        </div>
    )
}

export default Icon