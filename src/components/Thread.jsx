import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
// import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import { MdFavoriteBorder } from "react-icons/md";
import { FaRegComment } from "react-icons/fa";
import { IoShareSocial } from "react-icons/io5";
// import { MdExpandMore } from "react-icons/md";
// import { IoMdMore } from "react-icons/io";
import '../App.css'

const ExpandMore = styled((props) => {
  // eslint-disable-next-line no-unused-vars
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function RecipeReviewCard() {

//   const [expanded, setExpanded] = React.useState(false);
//   const handleExpandClick = () => {
// //     setExpanded(!expanded);
//   };

  return (
    <div className='inter container shadow-xl'>
        <Card className='max-w-full'>
            <CardHeader
                avatar={
                <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                    
                </Avatar>
                }
                action={
                <IconButton aria-label="settings">
                    {/* <IoMdMore /> */}
                </IconButton>
                }
                title="Shrimp and Chorizo Paella"
                subheader="September 14, 2016"
            />
            <CardMedia
                component="img"
                image="https://mui.com/static/images/cards/paella.jpg"
            />
            <CardContent>
                <div className='text-xs opacity-60'>
                This impressive paella is a perfect party dish and a fun meal to cook
                together with your guests. Add 1 cup of frozen peas along with the mussels,
                if you like.
                </div>
            </CardContent>
            <CardActions disableSpacing>
                <div className='flex flex-col'>
                    <div>
                        {/* <IconButton aria-label="like">
                            <MdFavoriteBorder size={25}  className='text-gray-800'/>
                        </IconButton> */}
                        {/* <IconButton aria-label="comment">
                            <FaRegComment size={22} className='text-gray-800'/>
                        </IconButton> */}
                        {/* <ExpandMore
                        //   expand={expanded}
                        //   onClick={handleExpandClick}
                        //   aria-expanded={expanded}
                        aria-label="share"
                        >
                            <IoShareSocial size={22} className='text-gray-800'/>
                        </ExpandMore> */}
                    </div>     

                    <div className='flex gap-2 p-2 text-xs text-gray-600'>
                        <span>Like 2k</span>  
                        <span>Comment 1.5k</span>
                    </div> 
                </div>
            </CardActions>
        </Card>
    </div>

  );
}