import {useState} from 'react';
import {IoEye, IoEyeOff, IoAdd, IoSwapHorizontal, IoTime} from "react-icons/io5";

const Card = () => {

    const [showBalance,
        setShowBalance] = useState(false);

    const toggleBalanceVisibility = () => {
        setShowBalance(!showBalance);
    }

    return (
        <div>
            <div
                className="card lg:flex flex-col bg-gradient-to-r from-green-400 via-blue-500 to-purple-600 text-white shadow-xl rounded-xl p-4 gap-4 border w-full h-20 lg:h-36 hidden">
                <div className="flex flex-row justify-between items-center gap">
                    <div className="font-semibold text-sm lg:text-2xl">K-Wallet</div>
                    <div>
                        <div className="text-white text-xs lg:text-sm">1234567890</div>
                    </div>
                </div>

                <div className="flex flex-row items-center gap-4">
                    {showBalance
                        ? (<IoEyeOff
                            className="hover:cursor-pointer"
                            size={20}
                            onClick={toggleBalanceVisibility}/>)
                        : (<IoEye
                            className="hover:cursor-pointer"
                            size={20}
                            onClick={toggleBalanceVisibility}/>)}
                    <div className="text-white text-xs lg:text-lg">
                        {showBalance
                            ? "Rp1.000.000"
                            : "••••••••••"}
                    </div>
                </div>
                <div
                    className="card lg:flex flex-col bg-gradient-to-r from-yellow-400 via-red-500 to-pink-500 text-white shadow-xl rounded-xl p-4 gap-4 border w-full h-48 hidden">
                    <div className="flex flex-row justify-between items-center">
                        <div className="font-semibold text-sm lg:text-xl">K-Points</div>
                        <div className="text-white text-sm lg:text-lg">3,450</div>
                    </div>
                </div>

                <div
                    className="card flex flex-row justify-around items-center bg-[#99BC85] text-white shadow-xl rounded-xl p-4 gap-3 border w-full h-48">
                    <div className='flex flex-col items-center gap-2 transition duration-100 ease-in-out transform hover:scale-110 hover:cursor-pointer'>
                        <IoAdd size={30}/>
                        <span className='text-white text-xs'>Top Up</span>
                    </div>
                    <div className='flex flex-col items-center gap-2 transition duration-100 ease-in-out transform hover:scale-110 hover:cursor-pointer'>
                        <IoSwapHorizontal size={30}/>
                        <span className='text-white text-xs'>Transfer</span>
                    </div>
                    <div className='flex flex-col items-center gap-2 transition duration-100 ease-in-out transform hover:scale-110 hover:cursor-pointer'>
                        <IoTime size={30}/>
                        <span className='text-white text-xs'>History</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Card;
