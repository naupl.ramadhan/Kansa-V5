const profileForm = () => {
    return (
        <div className="flex flex-col lg:grid grid-cols-2 gap-4">
            <input
                type="text"
                placeholder="Name"
                className="input input-bordered input-md w-full"/>
            <input
                type="text"
                placeholder="Class"
                className="input input-bordered input-md w-full"/>
            <input
                type="text"
                placeholder="Username"
                className="input input-bordered input-md w-full"/>
            <input
                type="number"
                placeholder="Phone"
                className="input input-bordered input-md w-full"/>
            <textarea
                placeholder="Short bio"
                className="textarea textarea-bordered w-full h-28 col-span-2"/>
        </div>
    )
}

export default profileForm;