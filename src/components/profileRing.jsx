const profileRing = () => {
    return (
        <div className="w-fit flex flex-col">
            <div className="avatar pt-3">
                <div
                    className="w-40 lg:w-56 rounded-full ring ring-green-500 ring-offset-base-100 ring-offset-2 hover:ring-gray-800">
                    <img src="https://miro.medium.com/v2/resize:fit:1134/0*4bjUTfx-yFBOn2Bd.jpg"/>
                </div>
            </div>
            <h3 className="flex mx-auto pt-5 text-lg lg:text-2xl">Anita Maxwynn</h3>
        </div>
    )
}

export default profileRing;