const historyList = () => {

    const orders = [
        {
            id: 1,
            name: "Hart Hagerty",
            date: "2023-06-15",
            total: "$150",
            description: "Zemlak, Daniel and Leannon - Desktop Support Technician",
            avatar: "https://img.daisyui.com/tailwind-css-component-profile-2@56w.png"
        }, {
            id: 2,
            name: "Brice Swyre",
            date: "2023-06-14",
            total: "$200",
            description: "Carroll Group - Tax Accountant",
            avatar: "https://img.daisyui.com/tailwind-css-component-profile-3@56w.png"
        }, {
            id: 3,
            name: "Marjy Ferencz",
            date: "2023-06-13",
            total: "$250",
            description: "Rowe-Schoen - Office Assistant I",
            avatar: "https://img.daisyui.com/tailwind-css-component-profile-4@56w.png"
        }, {
            id: 4,
            name: "Yancy Tear",
            date: "2023-06-12",
            total: "$100",
            description: "Wyman-Ledner - Community Outreach Specialist",
            avatar: "https://img.daisyui.com/tailwind-css-component-profile-5@56w.png"
        }, {
            id: 5,
            name: "Hart Hagerty",
            date: "2023-06-15",
            total: "$150",
            description: "Zemlak, Daniel and Leannon - Desktop Support Technician",
            avatar: "https://img.daisyui.com/tailwind-css-component-profile-2@56w.png"
        }, {
            id: 6,
            name: "Brice Swyre",
            date: "2023-06-14",
            total: "$200",
            description: "Carroll Group - Tax Accountant",
            avatar: "https://img.daisyui.com/tailwind-css-component-profile-3@56w.png"
        }, {
            id: 7,
            name: "Marjy Ferencz",
            date: "2023-06-13",
            total: "$250",
            description: "Rowe-Schoen - Office Assistant I",
            avatar: "https://img.daisyui.com/tailwind-css-component-profile-4@56w.png"
        }, {
            id: 8,
            name: "Yancy Tear",
            date: "2023-06-12",
            total: "$100",
            description: "Wyman-Ledner - Community Outreach Specialist",
            avatar: "https://img.daisyui.com/tailwind-css-component-profile-5@56w.png"
        }
    ];

    return (
        <div className="lg:hidden block overflow-auto w-full">
            <ul className="space-y-4">
                {orders.map((order) => (
                    <li
                        key={order.id}
                        className="bg-white shadow rounded-lg p-4 flex items-center space-x-4">
                        <div className="flex-shrink-0">
                            <img className="h-12 w-12 rounded-full" src={order.avatar} alt={order.name}/>
                        </div>
                        <div className="flex-1 min-w-0">
                            <div className="text-lg font-medium text-gray-900">{order.name}</div>
                            <div className="text-sm text-gray-500">{order.description}</div>
                            <div className="text-sm text-gray-500">{order.date}</div>
                        </div>
                        <div className="flex-shrink-0">
                            <span className="text-lg font-medium text-gray-900">{order.total}</span>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default historyList;