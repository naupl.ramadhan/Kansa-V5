import {Swiper, SwiperSlide} from 'swiper/react';
import {FreeMode} from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/pagination';
import burger from '../assets/burger.svg';
import ice from '../assets/iceCream.svg';
import drink from '../assets/drink.svg';
import fries from '../assets/fries.svg';
import chicken from '../assets/chicken.svg';
import cuisine from '../assets/cuisine.svg';
import cake from '../assets/cake.svg';

const mobileSwiper = () => {
    return (
        <div>
            <Swiper
                slidesPerView={'auto'}
                spaceBetween={10}
                grabCursor={true}
                freeMode={true}
                effect={'creative'}
                modules={[FreeMode]}
                className="mySwiper w-full h-full mb-10 overflow-hidden rounded-2xl relative block lg:hidden">
                <SwiperSlide style={{
                    width: 'auto'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 p-4 w-[100px] h-[130px] rounded-2xl">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3 transition duration-100 ease-in-out transform hover:scale-110'>
                            <img
                                src={burger}
                                alt="Burger"
                                className="icon w-14 mx-auto outline-none hover:outline"/>
                            <label className='flex text-xs items-center'>Meats</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: 'auto'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 p-4 w-[100px] h-[130px] rounded-2xl">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3 transition duration-100 ease-in-out transform hover:scale-110'>
                            <img
                                src={ice}
                                alt="Ice Cream"
                                className="icon w-14 mx-auto outline-none hover:outline"/>
                            <label className='text-xs'>Sweets</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: 'auto'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 p-4 w-[100px] h-[130px] rounded-2xl">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3 transition duration-100 ease-in-out transform hover:scale-110'>
                            <img
                                src={drink}
                                alt="Drink"
                                className="icon w-14 mx-auto outline-none hover:outline"/>
                            <label className='text-xs'>Drinks</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: 'auto'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 p-4 w-[100px] h-[130px] rounded-2xl">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3 transition duration-100 ease-in-out transform hover:scale-110'>
                            <img
                                src={fries}
                                alt="Fries"
                                className="icon w-14 mx-auto outline-none hover:outline"/>
                            <label className='text-xs'>Snacks</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: 'auto'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 p-4 w-[100px] h-[130px] rounded-2xl">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3 transition duration-100 ease-in-out transform hover:scale-110'>
                            <img
                                src={chicken}
                                alt="Chicken"
                                className="icon w-14 mx-auto outline-none hover:outline"/>
                            <label className='text-xs'>Chickens</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: 'auto'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 p-4 w-[100px] h-[130px] rounded-2xl">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3 transition duration-100 ease-in-out transform hover:scale-110'>
                            <img
                                src={cuisine}
                                alt="Cuisine"
                                className="icon w-14 mx-auto outline-none hover:outline"/>
                            <label className='text-xs'>Cuisines</label>
                        </a>
                    </div>
                </SwiperSlide>
                <SwiperSlide style={{
                    width: 'auto'
                }}>
                    <div
                        className="flex flex-col justify-center bg-gray-100 p-4 w-[100px] h-[130px] rounded-2xl">
                        <a
                            href=""
                            className='flex flex-col items-center gap-3 transition duration-100 ease-in-out transform hover:scale-110'>
                            <img
                                src={cake}
                                alt="Cake"
                                className="icon w-14 mx-auto outline-none hover:outline"/>
                            <label className='text-xs'>Cakes</label>
                        </a>
                    </div>
                </SwiperSlide>
            </Swiper>
        </div>
    )
}

export default mobileSwiper;
