import logo from "../images/logo.png";
import {AiFillInstagram} from "react-icons/ai";

function Footer() {
    return (
        <footer className="lg:flex flex-row justify-between hidden footer items-center p-2 pl-7 pr-7 bg-[#76885B] text-neutral-content">
            <aside className="items-center grid-flow-col">
                <img src={logo} className="w-14"/>
                <p className="text-white text-xs lg:text-sm">
                    <strong>Kansa</strong>
                    © 2023 - All rights reserved
                </p>
            </aside>
            <nav
                className="grid-flow-col gap-4 md:place-self-center md:justify-self-end text-white">
                <a href="https://www.instagram.com/naupl_/">
                    <AiFillInstagram size={35}/>
                </a>
            </nav>
        </footer>
    );
}

export default Footer;
