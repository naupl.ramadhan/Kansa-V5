import {
    CiHome,
    CiGrid41,
    CiClock2,
    CiLogout,
    CiUser,
    CiBellOn
} from "react-icons/ci";
import {useNavigate, useLocation} from "react-router-dom";

const BottomBar = () => {

    const navigate = useNavigate();
    const location = useLocation();

    const Dashboard = () => {
        navigate("/Dashboard");
    };
    const Menu = () => {
        navigate("/Menu");
    };
    const Notification = () => {
        navigate("/Notification");
    };
    const History = () => {
        navigate("/History");
    };
    const Profile = () => {
        navigate("/Profile");
    };
    const logoutHandler = () => {
        navigate("/Auth");
    };

    return (
        <div className="flex lg:hidden">
            <div className="btm-nav z-10">
                <button
                    className={location.pathname === "/Dashboard" ? "active" : ""}
                    onClick={Dashboard}
                >
                    <CiHome size="30"/>
                    <span className="btm-nav-label text-xs">Home</span>
                </button>
                <button
                    className={location.pathname === "/Menu" ? "active" : ""}
                    onClick={Menu}
                >
                    <CiGrid41 size="30"/>
                    <span className="btm-nav-label text-xs">Menu</span>
                </button>
                <button
                    className={location.pathname === "/Notification" ? "active" : ""}
                    onClick={Notification}
                >
                    <CiBellOn size="30"/>
                    <span className="btm-nav-label text-xs">Notifications</span>
                </button>
                <button
                    className={location.pathname === "/History" ? "active" : ""}
                    onClick={History}
                >
                    <CiClock2 size="30"/>
                    <span className="btm-nav-label text-xs">History</span>
                </button>
                <button
                    className={location.pathname === "/Profile" ? "active" : ""}
                    onClick={Profile}
                >
                    <CiUser size="30"/>
                    <span className="btm-nav-label text-xs">Profile</span>
                </button>
            </div>
        </div>
    );
};

export default BottomBar;