import BottomBar from '../../components/BottomBar';
import SearchBar from '../../components/SearchBar';
import Swiper from '../../components/Swiper';
import Card from '../../components/Card';
import Anigas from '../../components/menuSwiper';
import Amogus from '../../components/mobileSwiper';
import Sidebar from '../../components/Sidebar';
import Thread from '../../components/Thread';
import Footer from '../../components/Footer';
import '../../App.css'

const Dashboard = () => {
    return (
        <div className='w-full h-full inter bg-[#EEEEEE]'>
            {/* navigation */}
            <Sidebar/>
            <BottomBar/>
            <div className='mb-8'>
                <SearchBar/>
                <div className='pt-8 lg:p-10 lg:pl-44 lg:pb-0 pb-16 -z-10'>
                    <div className='p-8 lg:rounded-2xl rounded-[30px] bg-[#FFFFFF] shadow-xl'>
                        <p className='lg:text-2xl text-2xl font-bold mb-4'>What's new?</p>
                        <div className='flex lg:flex-row flex-col w-full lg:gap-10 gap-5'>
                            <div className='lg:w-8/12 w-full'><Swiper/></div>
                            <div className='hidden lg:flex flex-col w-full'>
                                <p className='lg:flex lg:text-2xl text-sm font-bold mb-4 -mt-2 hidden'>My Balance</p>
                                <div className='w-full'><Card/></div>
                            </div>
                        </div>
                        <p
                            className='flex justify-center text-sm lg:text-xl font-bold lg:pt-0 pt-8 mb-4'>Get started from our selection!</p>
                        <div className='lg:flex hidden w-full h-full justify-center'><Anigas/></div>
                        <div className='lg:hidden block w-full h-full'><Amogus/></div>
                        <p className='lg:text-2xl text-md font-bold mb-4'>Recently happened</p>

                        {/*  Mobile */}
                        <div className='lg:hidden block rounded-[20px] overflow-hidden outline-dotted outline-offset-2'>
                            <div className='h-[500px] overflow-y-scroll'>
                                <div className='container'>
                                    <div className='grid grid-cols-1 gap-4'>
                                        <Thread/>
                                        <Thread/>
                                        <Thread/>
                                        <Thread/>
                                        <Thread/>
                                        <Thread/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* Dekstop */}
                        <div className='hidden lg:grid grid-cols-1 lg:grid-cols-3 gap-4'>
                            <Thread/>
                            <Thread/>
                            <Thread/>
                            <Thread/>
                            <Thread/>
                            <Thread/>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
}

export default Dashboard;
