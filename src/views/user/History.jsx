import SearchBar from "../../components/SearchBar"
import Sidebar from "../../components/Sidebar"
import Table from "../../components/Table"
import TabList from "../../components/tabList"
import HistoryList from "../../components/historyList"
import Pagination from "../../components/Pagination"
import Thread from "../../components/Thread"
import BottomBar from "../../components/BottomBar"
import Footer from "../../components/Footer"
import '../../App.css'

const History = () => {
    return (
        <div className="flex flex-col w-full h-full lg:h-screen inter bg-[#EEEEEE]">
            <SearchBar/>
            < Sidebar/>
            <div className="hidden w-full h-full lg:block lg:pt-5 lg:pl-40 lg:pr-10">
                <div className="hidden lg:flex flex-row gap-5">
                    <div
                        className="w-3/4 h-full p-8 shadow-xl rounded-2xl bg-[#FFFFFF]">
                        <div className="text-4xl font-bold mb-3">Order History</div>
                        <div className="flex flex-row justify-between w-full mb-4">
                            <div className="lg:flex hidden"><TabList/></div>
                            <div className="lg:flex hidden"><Pagination/></div>
                        </div>
                        <div className="lg:block hidden rounded-2xl"><Table/></div>
                    </div>
                    <div
                        className="hidden lg:flex flex-col text-xl gap-3 p-4 shadow-xl rounded-2xl bg-[#FFFFFF]">
                        <p className="">Last reviewed</p>
                        <div
                            className="flex rounded-[20px] overflow-hidden outline-dotted outline-offset-2">
                            <div className='h-[375px] overflow-y-scroll'>
                                <div className="max-w-[300px]">
                                    <div className="flex flex-col gap-4">
                                        <Thread/>
                                        <Thread/>
                                        <Thread/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <BottomBar/>
            <div className="pt-6 pb-20 lg:pt-0 lg:pb-0"><HistoryList/></div>
            <Footer/>
        </div>
    )
}

export default History;