import ProfileRing from "../../components/profileRing";
import ProfileForm from "../../components/profileForm";
import ProfileBalance from "../../components/profileBalance";
import BottomBar from "../../components/BottomBar";
import Footer from "../../components/Footer";
import '../../App.css'

const Profile = () => {
    return (
        <div
            className="w-full min-h-screen inter bg-[#EEEEEE] flex flex-col justify-between">
            <div className="flex flex-col lg:flex-row pt-8 lg:pl-20">
                <div className="mx-auto"><ProfileRing/></div>
                <div className="flex flex-col w-full h-full lg:pl-10 lg:pr-20 gap-5">
                    <div>
                        <div className="hidden pl-5 lg:flex lg:pl-0 text-2xl lg:text-5xl font-bold">Your Profile</div>
                    </div>

                    <div
                        className="w-full h-full shadow-xl rounded-[30px] p-5 lg:p-4 mb-24 lg:mb-10 bg-[#FFFFFF]">
                        <div className="flex flex-col gap-4">
                            <div className="font-semibold">About me!</div>
                            <ProfileForm/>
                            <div className="font-semibold">My balance</div>
                            <ProfileBalance/>
                        </div>
                    </div>
                </div>
            </div>
            <BottomBar/>
            <Footer/>
        </div>
    );
}

export default Profile;
