import FormLogin from "../../components/formLogin";
import splashScreen from "../../images/splashScreen.png";
import logo from "../../images/logo.png";
import logoNegative from "../../images/logoNegative.png";
import '../../App.css';

const Login = () => {
    return (
        <div
            className="w-full h-full inter bg-[#EEEEEE] flex items-center justify-center">
            <div
                className="w-full h-screen flex flex-row items-center justify-center mx-auto">
                <img src={splashScreen} className="hidden lg:block w-3/5 h-full object-cover"/>
                <div
                    className="flex flex-col w-full lg:w-1/2 h-full justify-center items-center lg:pt-8 bg-gray-100 dark:bg-[#2B2B2B] shadow-2xl">
                    <img
                        src={logo}
                        className="w-[200px] h-[200px] lg:w-[250px] lg:h-[250px] mx-auto hidden dark:flex"/>
                    <img
                        src={logoNegative}
                        className="w-[200px] h-[200px] lg:w-[250px] lg:h-[250px] mx-auto flex dark:hidden"/>
                    <FormLogin/>
                </div>
            </div>
        </div>
    );
};

export default Login;
